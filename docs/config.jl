using IgaFormation

package_info = Dict(
    "modules" => [IgaFormation],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "IgaFormation.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/IgaFormation",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(IgaFormation, :DocTestSetup, :(using IgaFormation); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(IgaFormation, fix=true)
end
